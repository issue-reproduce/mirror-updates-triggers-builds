module Errors
  class UnauthorizedError < BaseError
    def code
      401
    end

    def title
      'Unauthorized'
    end

    def detail
      'You are not allowed to access that resource'
    end
  end
end
