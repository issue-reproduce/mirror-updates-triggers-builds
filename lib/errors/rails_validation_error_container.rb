module Errors
  class RailsValidationErrorContainer < ErrorContainer
    def initialize(errors)
      convert(errors)
    end

    private

    def convert(errors)
      @errors = errors.keys.map do |error_key|
        errors[error_key].map do |error|
          Errors::RailsValidationError.new(error_key.to_s, error)
        end
      end.flatten
    end
  end
end
