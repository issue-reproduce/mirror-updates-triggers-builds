module Errors  
  class BaseError
    attr_reader :code, :title, :detail

    def to_hash
      {
        code: code,
        title: title,
        detail: detail
      }
    end
  end
end
