module Errors
  class InternalServerError < BaseError
    def code
      500
    end

    def title
      'Internal Server Error'
    end

    def detail
      'Something went wrong!! Please try again'
    end
  end
end
