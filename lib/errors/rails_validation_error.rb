module Errors  
  class RailsValidationError < BaseError
    def initialize(type, description)
      @title, @detail = type, description
    end

    def code
      422
    end
  end
end
