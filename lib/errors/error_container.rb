module Errors
  class ErrorContainer
    attr_reader :errors

    def initialize(*errors)
      @errors = errors.flatten
    end

    def to_a
      errors.map { |error| error.to_hash }
    end
  end
end
