module Errors
  class ForbiddenError < BaseError
    def code
      403
    end

    def title
      'Forbidden'
    end

    def detail
      'This request is not supported'
    end
  end
end
