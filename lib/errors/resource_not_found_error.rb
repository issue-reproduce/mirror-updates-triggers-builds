module Errors  
  class ResourceNotFoundError < BaseError
    attr_reader :type, :id

    def initialize(type, id)
      @type, @id = type, id
    end

    def code
      404
    end

    def title
      "Resource not found"
    end

    def detail
      "You asked for #{type} with ID:#{id}, but this does not exist"
    end
  end
end
