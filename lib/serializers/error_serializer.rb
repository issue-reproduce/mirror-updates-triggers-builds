module Serializers
  class ErrorSerializer
    attr_reader :errors

    def initialize(error_container)
      @errors = error_container
    end

    def serialize
      { errors: errors.to_a }
    end
  end
end
