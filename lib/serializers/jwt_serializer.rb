module Serializers
  class JwtSerializer < ActiveModel::Serializer
    type 'auth_token'

    attribute :jwt do
      JWT.encode jwt_payload, JwtSerializer.secret, JwtSerializer.alg
    end

    def jwt_payload
      { user_id: object.id }
    end

    def self.alg
      'HS256'
    end

    def self.secret
      Rails.application.secrets.jwt_client_secret
    end

    def self.deserialize(token)
      begin
        contents = JWT.decode token, secret, alg
        contents[0].to_hash
      rescue JWT::DecodeError
        nil
      end
    end
  end
end
