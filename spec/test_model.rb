class TestModel
  include ActiveModel::Validations

  def initialize(opts = {})
    @name = opts[:name]
  end

  attr_accessor :name

  validates_presence_of :name
end
