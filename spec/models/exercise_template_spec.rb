require 'rails_helper'

RSpec.describe ExerciseTemplate, type: :model do
  fixtures :users, :routine_templates

  let(:creator) { users(:user) }
  let(:routine_template) { routine_templates(:routine_template) }

  let(:attr) do
    {
      exercise_name:'Bench Press',
      starting_criteria: {
        weight: 20,
        sets: 5
      },
      completion_criteria: {
        threshold: 3,
        weight: 120
      },
      progression_criteria: {
        threshold: 1,
        weight: 2.5,
        reps: 0
      },
      failure_criteria: {
        threshold: 3,
        weight: 2.5,
        reps: 0
      },
      routine_template: routine_template,
      creator: creator
    }
  end
  let(:exercise_template) { ExerciseTemplate.new(attr) }

  it 'should be valid' do
    expect(exercise_template).to be_valid
  end

  it 'should be invalid without a exercise name' do
    exercise_template.exercise_name = nil
    expect(exercise_template).to_not be_valid
  end

  it 'should be invalid without a routine_template' do
    exercise_template.routine_template = nil
    expect(exercise_template).to_not be_valid
  end

  it 'should be invalid without a creator' do
    exercise_template.creator = nil
    expect(exercise_template).to_not be_valid
  end

  it 'should be invalid without starting_criteria' do
    exercise_template.starting_criteria = nil
    expect(exercise_template).to_not be_valid
  end

  it 'should be invalid without completion_criteria' do
    exercise_template.completion_criteria = nil
    expect(exercise_template).to_not be_valid
  end

  it 'should be invalid without progression_criteria' do
    exercise_template.progression_criteria = nil
    expect(exercise_template).to_not be_valid
  end

  it 'should be invalid without failure_criteria' do
    exercise_template.failure_criteria = nil
    expect(exercise_template).to_not be_valid
  end

  context 'validate nested criteria' do
    context 'starting_criteria' do
      it 'should be invalid without a weight' do
        exercise_template.starting_criteria = {
          sets: 5
        }
        expect(exercise_template).to be_invalid
      end

      it 'should be invalid with a nil weight' do
        exercise_template.starting_criteria = {
          sets: 5,
          weight: nil
        }
        expect(exercise_template).to be_invalid
      end

      it 'should be invalid without a sets' do
        exercise_template.starting_criteria = {
          weight: 20
        }
        expect(exercise_template).to be_invalid
      end

      it 'should be invalid with a nil sets' do
        exercise_template.starting_criteria = {
          weight: 20,
          sets: nil
        }
        expect(exercise_template).to be_invalid
      end
    end

    context 'completion_criteria' do
      it 'should be invalid without a threshold' do
        exercise_template.completion_criteria = {
          weight: 5
        }
        expect(exercise_template).to be_invalid
      end

      it 'should be invalid with a nil threshold' do
        exercise_template.completion_criteria = {
          weight: 5,
          threshold: nil
        }
        expect(exercise_template).to be_invalid
      end

      it 'should be invalid without a weight' do
        exercise_template.completion_criteria = {
          threshold: 20
        }
        expect(exercise_template).to be_invalid
      end

      it 'should be invalid with a nil weight' do
        exercise_template.completion_criteria = {
          threshold: 20,
          weight: nil
        }
        expect(exercise_template).to be_invalid
      end
    end

    context 'progression_criteria' do
      it 'should be invalid without a threshold' do
        exercise_template.progression_criteria = {
          weight: 5,
          reps: 5
        }
        expect(exercise_template).to be_invalid
      end

      it 'should be invalid with a nil threshold' do
        exercise_template.progression_criteria = {
          weight: 5,
          threshold: nil,
          reps: 5
        }
        expect(exercise_template).to be_invalid
      end

      it 'should be invalid without a weight' do
        exercise_template.progression_criteria = {
          threshold: 20,
          reps: 5,
        }
        expect(exercise_template).to be_invalid
      end

      it 'should be invalid with a nil weight' do
        exercise_template.progression_criteria = {
          threshold: 20,
          weight: nil,
          reps: 5
        }
        expect(exercise_template).to be_invalid
      end

      it 'should be invalid without a reps' do
        exercise_template.progression_criteria = {
          threshold: 20,
          weight: 5
        }
        expect(exercise_template).to be_invalid
      end

      it 'should be invalid with a nil reps' do
        exercise_template.progression_criteria = {
          threshold: 20,
          weight: 5,
          reps: nil
        }
        expect(exercise_template).to be_invalid
      end
    end

    context 'failure_criteria' do
      it 'should be invalid without a threshold' do
        exercise_template.failure_criteria = {
          weight: 5,
          reps: 5
        }
        expect(exercise_template).to be_invalid
      end

      it 'should be invalid with a nil threshold' do
        exercise_template.failure_criteria = {
          weight: 5,
          threshold: nil,
          reps: 5
        }
        expect(exercise_template).to be_invalid
      end

      it 'should be invalid without a weight' do
        exercise_template.failure_criteria = {
          threshold: 20,
          reps: 5
        }
        expect(exercise_template).to be_invalid
      end

      it 'should be invalid with a nil weight' do
        exercise_template.failure_criteria = {
          threshold: 20,
          weight: nil,
          reps: 5
        }
        expect(exercise_template).to be_invalid
      end

      it 'should be invalid without a reps' do
        exercise_template.failure_criteria = {
          threshold: 20,
          weight: 5
        }
        expect(exercise_template).to be_invalid
      end

      it 'should be invalid with a nil reps' do
        exercise_template.failure_criteria = {
          threshold: 20,
          weight: 5,
          reps: nil
        }
        expect(exercise_template).to be_invalid
      end
    end
  end
end
