require 'rails_helper'

RSpec.describe RoutineTemplate, type: :model do
  fixtures :users, :program_templates

  let(:creator) { users(:user) }
  let(:program_template) { program_templates(:program_template) }

  let(:attr) do
    {
      name:'Routine A',
      description:'My routine',
      program_template: program_template,
      creator: creator
    }
  end
  let(:routine_template) { RoutineTemplate.new(attr) }

  it 'should be valid' do
    expect(routine_template).to be_valid
  end

  it 'should be invalid without a name' do
    routine_template.name = nil
    expect(routine_template).to_not be_valid
  end

  it 'should be invalid without a program_template' do
    routine_template.program_template = nil
    expect(routine_template).to_not be_valid
  end

  it 'should be invalid without a creator' do
    routine_template.creator = nil
    expect(routine_template).to_not be_valid
  end

  it 'should respond to exercise_templates' do
    expect(routine_template).to respond_to(:exercise_templates)
  end
end
