require 'rails_helper'

RSpec.describe User, type: :model do
  fixtures(:users)

  let(:attr) do
    {
      email:'a@b.com',
      password:'password',
      password_confirmation:'password'
    }
  end
  let(:user) { User.new(attr) }

  it 'should be invalid without an email' do
    user.email = nil
    expect(user).to_not be_valid
  end

  it 'should be invalid without a unique email' do
    user.save
    duplicate_email_user = User.new(attr)
    expect(duplicate_email_user).to_not be_valid
    expect(duplicate_email_user.errors.full_messages).to include('Email has already been taken')
  end

  context 'on create' do
    it 'should be invalid without a password' do
      user.password = nil
      expect(user).to_not be_valid
    end

    it 'should be invalid without a password_confirmation' do
      user.password_confirmation = nil
      expect(user).to_not be_valid
    end
  end

  context 'after create' do
    before { user.save }

    it 'should be valid without a password' do
      user.password = nil
      expect(user).to be_valid
    end

    it 'should be valid without a password_confirmation' do
      user.password_confirmation = nil
      expect(user).to be_valid
    end
  end


  it 'should be invalid without a matching confirmation' do
    user.password_confirmation = 'invalid'  
    expect(user).to_not be_valid
  end

  context '#password_matches?' do
    before { user.save }

    it 'should return true for a password match' do
      expect(user.password_matches?(attr[:password])).to eq(true)
    end

    it 'should return false for a password mismatch' do
      expect(user.password_matches?('incorrect')).to eq(false)
    end
  end

  context 'encryption' do
    before { user.save }

    it 'should generate a salt' do
      expect(user.salt).to_not be_nil
    end

    it 'should generate an encrypted_password' do
      expect(user.encrypted_password).to_not be_nil
    end

    it 'should verify a password' do
      expect(User.authenticate(attr[:email],attr[:password])).to be_truthy
    end

    it 'doesnt generate a new salt on resave' do
      first_salt = user.salt
      user.save
      expect(user.salt).to eq(first_salt)
    end
  end

  context('::generate_salt') do
    it 'should generate a new string every time' do
      expect(User.generate_salt).to_not eq(User.generate_salt)
    end
  end

  context('::encrypt_password') do
    context('with the same password and salt') do
      let(:salt) { 'abc' }
      let(:password) { 'abc' }

      it 'should generate the same string every time' do
        expect(User.encrypt_password(password,salt)).to eq(User.encrypt_password(password,salt))
      end

      it 'should generate a different string with a different salt' do
        expect(User.encrypt_password(password,salt + 'a')).to_not eq(User.encrypt_password(password,salt))
      end
    end
  end

  context '::authenticate' do
    before { user.save }

    it 'should authenticate a user with the correct credentials' do
      expect(User.authenticate(attr[:email], attr[:password])).to eq(user)
    end

    it 'should return false for a user with incorrect credentials' do
      expect(User.authenticate(attr[:email], 'incorrect')).to eq(false)
    end

    it 'should return false for an email that doesnt exist' do
      expect(User.authenticate('no_user@gone.com', attr[:password])).to eq(false)
    end
  end

  context '::by_auth_token' do
    let(:user) { users(:user) }

    context 'given a valid token' do
      let(:token) { Serializers::JwtSerializer.new(user).attributes[:jwt] }

      it 'should return the user contained within the token' do
        expect(User.by_auth_token(token)).to eq(user)
      end
    end

    context 'given an invalid token' do
      let(:token) { 'gumpf'}

      it 'should return nil' do
        expect(User.by_auth_token(token)).to eq(nil)
      end
    end
  end
end
