require 'rails_helper'

RSpec.describe ProgramTemplate, type: :model do
  fixtures :users

  let(:creator) { users(:user) }
  let(:attr) do
    {
      name:'Program Name',
      acronym:'PNA',
      description:'Program Description',
      suggested_length: 12345,
      creator: creator
    }
  end
  let(:program_template) { ProgramTemplate.new(attr) }

  it 'should be invalid without a name' do
    program_template.name = nil
    expect(program_template).to_not be_valid
  end

  it 'should be invalid without an acronym' do
    program_template.acronym = nil
    expect(program_template).to_not be_valid
  end

  it 'should be invalid without a description' do
    program_template.description = nil
    expect(program_template).to_not be_valid
  end

  it 'should be invalid without a suggested_length' do
    program_template.suggested_length = nil
    expect(program_template).to_not be_valid
  end

  it 'should be invalid if name is not unique' do
    new_name = program_template.name = 'New Name'
    program_template.save
    program_template_2 = ProgramTemplate.new(attr.merge(name: new_name, acronym: 'different'))
    expect(program_template_2).to_not be_valid
  end

  it 'should be invalid if acronym is not unique' do
    new_acronym = program_template.acronym = 'NA'
    program_template.save
    program_template_2 = ProgramTemplate.new(attr.merge(acronym: new_acronym, name: 'different'))
    expect(program_template_2).to_not be_valid
  end

  it 'should be invalid without a creator' do
    program_template.creator = nil
    expect(program_template).to_not be_valid
  end

  it 'should respond to routine_templates' do
    expect(program_template).to respond_to(:routine_templates)
  end
end
