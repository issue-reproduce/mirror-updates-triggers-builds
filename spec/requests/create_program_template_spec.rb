require 'rails_helper'

RSpec.describe "CreateProgramTemplateSpec", type: :request do
  fixtures :users, :program_templates

  let(:program_template_attr) do
    {
      "data": {
        "type": "program_templates",
        "attributes": {
          "name": "StrongLifts 5x5",
          "acronym": "SL5x5",
          "description": "Stronglifts 5×5 is the simplest, most effective workout to get stronger, build muscle and burn fat.
Read more: http://stronglifts.com/5x5/",
          "suggested_length": "3 months",
          "creator_id": user.id
        }
      }
    }
  end
  let(:user) { users(:user) }
  let(:program_template) { program_templates(:program_template) }
  let(:token) { login_user(user, 'password') }
  let(:auth_header) do
    { 'HTTP_AUTHORIZATION' => "Token #{token}" }
  end

  describe "POST /program_templates" do
    it "creation should return CREATED and the new resource" do
      post '/program_templates', program_template_attr, auth_header
      expect(response).to have_http_status(201)
      expect(response_json(response)['data']['id']).to be_present
      expect(response_json(response)['data']['attributes']['creator_id']).to be_present
    end
  end

  describe "GET /program_templates" do
    it "should return all program_templates" do
      get '/program_templates', nil, auth_header
      expect(response).to have_http_status(200)
      expect(response_json(response)['data']).to be_a(Array)
    end
  end

  describe "GET /program_templates/:id" do
    it 'should return the correct program_template' do
      get "/program_templates/#{program_template.id}", nil, auth_header
      expect(response).to have_http_status(200)
      expect(response_json(response)['data']['attributes']['name']).to eq(program_template.name)
    end
  end

  describe "GET /program_templates/edit/:id" do
    it 'should return the correct program_template to edit' do
      get "/program_templates/#{program_template.id}/edit", nil, auth_header
      expect(response).to have_http_status(200)
      expect(response_json(response)['data']['attributes']['name']).to eq(program_template.name)
    end
  end

  describe "PATCH /program_templates/:id" do
    let(:new_name) { 'New Name' }
    let(:update_attr) do
      program_template_attr.deep_dup.tap do |new_attr|
        new_attr[:data].merge!(attributes: {name: new_name})
      end
    end

    it 'update should return OK and have the correct attributes' do
      patch "/program_templates/#{program_template.id}", update_attr, auth_header
      expect(response).to have_http_status(200)
      expect(response_json(response)['data']['id']).to be_present
      expect(response_json(response)['data']['attributes']['name']).to eq(new_name)
    end
  end

  describe "DELETE /program_templates/:id" do
    it 'should return OK' do
      delete "/program_templates/#{program_template.id}", nil, auth_header
      expect(response).to have_http_status(204)
    end
  end
end
