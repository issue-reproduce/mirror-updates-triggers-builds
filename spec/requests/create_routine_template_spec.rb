require 'rails_helper'

RSpec.describe "CreateRoutineTemplateSpec", type: :request do
  fixtures :users, :program_templates, :routine_templates

  let(:routine_template_attr) do
    {
      "data": {
        "type": "routine_templates",
        "attributes": {
          "name": "Routine A",
          "description": "My routine"
        }
      }
    }
  end
  let(:user) { users(:user) }
  let(:program_template) { program_templates(:program_template) }
  let(:routine_template) { routine_templates(:routine_template) }
  let(:token) { login_user(user, 'password') }
  let(:auth_header) do
    { 'HTTP_AUTHORIZATION' => "Token #{token}" }
  end

  describe 'POST /program_templates/:id/routine_templates' do
    it 'should return CREATED and the new resource' do
      post "/program_templates/#{program_template.id}/routine_templates", routine_template_attr, auth_header
      expect(response).to have_http_status(:created)
      expect(response_json(response)['data']['id']).to be_present
    end
  end

  describe 'GET /program_templates/:id/routine_templates' do
    it 'should return SUCCESS and the list of resources' do
      get "/program_templates/#{program_template.id}/routine_templates", nil, auth_header
      expect(response).to have_http_status(:success)
      expect(response_json(response)['data']).to be_a(Array)
    end
  end
end
