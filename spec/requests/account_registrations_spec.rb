require 'rails_helper'

RSpec.describe "AccountRegistrations", type: :request do
  let(:attr) do
    { 
      "data": {
        "type": "users",
        "attributes": {
          "email": "a@b.com",
          "password": "password",
          "password_confirmation": "password"
        },
        "relationships": {
        },
        "links": {
        },
        "meta": {
        }
      }
    }
  end

  describe "POST /register" do
    it "works! (now write some real specs)" do
      post register_path, attr
      expect(response).to have_http_status(201)
    end
  end
end
