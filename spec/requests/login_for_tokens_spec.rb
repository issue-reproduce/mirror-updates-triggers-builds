require 'rails_helper'

RSpec.describe "RegisterAndLoginForTokens", type: :request do
  let(:registration_attr) do
    {
      "data": {
        "type": "users",
        "attributes": {
          "email": "a@b.com",
          "password": "password",
          "password_confirmation": "password"
        }
      }
    }
  end

  let(:login_attr) do
    {
      "data": {
        "type": "users",
        "attributes": {
          "email": "a@b.com",
          "password": "password"
        }
      }
    }
  end


  describe "POST /login" do
    it "register then login should return a token" do
      post register_path, registration_attr
      expect(response).to have_http_status(201)

      post login_path, login_attr
      expect(response).to have_http_status(200)

      expect(response_json(response)['data']['attributes']['jwt']).to be_present
    end
  end
end
