require 'rails_helper'

RSpec.describe "EndToEndSpec", type: :request do
  before do
    @auth_attr =
    {
      "data": {
        "type": "users",
        "attributes": {
          "email": "a@b.com",
          "password": "password",
          "password_confirmation": "password"
        }
      }
    }
    @program_template_attr =
    {
      "data": {
        "type": "program_templates",
        "attributes": {
          "name": "StrongLifts 5x5",
          "acronym": "SL5x5",
          "description": "Stronglifts 5×5 is the simplest, most effective workout to get stronger, build muscle and burn fat.
Read more: http://stronglifts.com/5x5/",
          "suggested_length": "3 months",
        }
      }
    }
    @routine_attr =
    {
      "data": {
        "type": "routine_template",
        "attributes": {
          "name": "Routine A",
          "description": "My routine"
        }
      }
    }
    @exercise_attr =
    {
      "data": {
        "type": "exercise_template",
        "attributes": {
          "exercise_name": "Bench Press",
          "starting_criteria": {
            "weight": 20,
            "sets": 5
          },
          "completion_criteria": {
            "threshold": 3,
            "weight": 120
          },
          "progression_criteria": {
            "threshold": 1,
            "weight": 2.5,
            "reps": 0
          },
          "failure_criteria": {
            "threshold": 3,
            "weight": 2.5,
            "reps": 0
          }
        }
      }
    }
  end

  describe 'Full on Program creation' do
    context 'bare program templates' do
      it 'should work' do
        register
        program_template_json = create_program_template
        visit_program_template(program_template_json["id"])
      end
    end

    context 'program with one routine' do
      it 'should work' do
        register
        program_template_id = create_program_template["id"]
        routine_template_id = create_routine_for_program(program_template_id)["id"]
        exercise_template_id = create_exercise_for_routine(routine_template_id)
      end
    end

    context 'program with multiple routines' do
      it 'should work' do
        register
        program_template_id = create_program_template["id"]
        routine_template_a_id = create_routine_for_program(program_template_id)["id"]
        exercise_template_a_id = create_exercise_for_routine(routine_template_a_id)
        routine_template_b_id = create_routine_for_program(program_template_id)["id"]
        exercise_template_b_id = create_exercise_for_routine(routine_template_b_id)
      end
    end
  end
end

def register
  post '/users', @auth_attr
  expect(response).to have_http_status(:created)
end

def visit_program_template(id)
  get "/program_templates/#{id}", nil, auth_header
  expect(response).to have_http_status(:ok)
end

def create_program_template
  post '/program_templates', @program_template_attr, auth_header
  assert_created
  response_json(response)['data']
end

def create_routine_for_program(program_id)
  post "/program_templates/#{program_id}/routine_templates", @routine_attr, auth_header
  assert_created
  response_json(response)["data"]
end

def create_exercise_for_routine(routine_id)
  post "/routine_templates/#{routine_id}/exercise_templates", @exercise_attr, auth_header
  assert_created
  response_json(response)["data"]
end

def assert_created
  expect(response).to have_http_status(:created)
end

def token
  @token ||= login_username(
    @auth_attr[:data][:attributes][:email],
    @auth_attr[:data][:attributes][:password])
end

def auth_token
  "Token #{token}"
end

def auth_header
  { 'HTTP_AUTHORIZATION' => auth_token }
end
