require 'rails_helper'

RSpec.describe "AuthenticatedRoutes", type: :request do
  fixtures(:users)

  let(:user) { users(:user) }
  let(:token) { login_user(user, 'password')}

  describe "jwt based authentication" do
    it 'should respond success with the authorization header' do
      get "/users/#{user.id}", nil, { 'HTTP_AUTHORIZATION' => "Token #{token}" }
      expect(response).to have_http_status(200)
    end

    it 'should respond unauthorised with an incorrect auth header' do
      get "/users/#{user.id}", nil, { 'HTTP_AUTHORIZATION' => "Token gumpf" }
      expect(response).to have_http_status(401)
    end

    it 'should respond unauthorised without an auth header' do
      get "/users/#{user.id}", nil, { 'HTTP_AUTHORIZATION' => nil }
      expect(response).to have_http_status(401)
    end
  end

  describe "determine the current user" do
    it 'should respond with the current user information' do
      get "/logged_in_user", nil, {'HTTP_AUTHORIZATION' => "Token #{token}"}
      expect(response).to have_http_status(200)
      expect(response_json(response)["data"]["type"]).to eq("users")
      expect(response_json(response)["data"]["attributes"]["email"]).to eq(user.email)
    end
  end
end
