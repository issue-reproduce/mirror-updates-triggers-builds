require 'rails_helper'

RSpec.describe Serializers::JwtSerializer do
  fixtures :users

  let(:user) { users(:user) }
  let(:serializer) { Serializers::JwtSerializer.new(user) }

  it 'should have attributes containing the jwt' do
    expect(serializer.attributes.keys).to include(:jwt) 
  end

  it 'should contain a populated jwt' do
    expect(serializer.attributes[:jwt]).to be_present
  end

  context '::deserialize' do
    let(:token) { serializer.attributes[:jwt] }

    it 'should return a hash' do
      expect(Serializers::JwtSerializer.deserialize(token)).to be_a(Hash)
    end

    it 'should have the contain the user_id' do
      expect(Serializers::JwtSerializer.deserialize(token)['user_id']).to be_present
      expect(Serializers::JwtSerializer.deserialize(token)['user_id']).to eq(user.id)
    end
  end
end
