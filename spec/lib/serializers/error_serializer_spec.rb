require 'rails_helper'
require 'test_error'
require 'test_model'

RSpec.describe Serializers::ErrorSerializer do
  context 'for a regular error container' do
    let(:errors) do
      Errors::ErrorContainer.new(TestError.new)
    end

    let(:serializer) { Serializers::ErrorSerializer.new(errors) }

    it 'should serialize the errors correctly' do
      expect(serializer.serialize).to eq(
        {
          errors: [
            {
              code: 0,
              title: 'My Title',
              detail: 'My details'
            }
          ] 
        }
      )
    end
  end

  context 'for a rails validation error container' do
    let(:test_model) { TestModel.new }
    let(:errors) do
      test_model.valid?
      Errors::RailsValidationErrorContainer.new(test_model.errors)
    end

    let(:serializer) { Serializers::ErrorSerializer.new(errors) }

    it 'should serialize the errors correctly' do
      expect(serializer.serialize).to eq(
        {
          errors: [
            {
              code: 422,
              title: 'name',
              detail: "can't be blank"
            }
          ] 
        }
      )
    end
  end
end
