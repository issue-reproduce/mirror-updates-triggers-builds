require 'rails_helper'
require 'test_model'

RSpec.describe Errors::RailsValidationErrorContainer do
  let(:invalid_model) { TestModel.new }
  let(:valid_model) { TestModel.new(name: 'Name') }
  context '#convert' do
    context 'for an invalid model' do
      before { invalid_model.valid? }

      it 'should return an array of RailsValidationErrors' do
        container = Errors::RailsValidationErrorContainer.new(invalid_model.errors)
        expect(container.errors).to be_present
        expect(container.errors.first).to be_a(Errors::RailsValidationError)
      end
    end

    context 'for a valid model' do
      before { valid_model.valid? }

      it 'should return an empty array' do
        container = Errors::RailsValidationErrorContainer.new(valid_model.errors)
        expect(container.errors).to eq([])
      end
    end
  end
end
