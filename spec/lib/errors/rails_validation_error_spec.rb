require 'rails_helper'

RSpec.describe Errors::RailsValidationError do
  let(:field) { 'email' }
  let(:description) { "can't be blank" }
  let(:rails_validation_error) { Errors::RailsValidationError.new(field, description) }

  it 'should have the correct code' do
    expect(rails_validation_error.code).to eq(422)
  end

  it 'should have the correct title' do
    expect(rails_validation_error.title).to eq(field)
  end

  it 'should have the correct details' do
    expect(rails_validation_error.detail).to eq(description)
  end

  it 'should return the correct hash of detail' do
    expect(rails_validation_error.to_hash).to eq(
      {
        code: 422,
        title: field,
        detail: description
      }
    )
  end
end
