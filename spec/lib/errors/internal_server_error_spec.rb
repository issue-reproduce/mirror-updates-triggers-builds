require 'rails_helper'

RSpec.describe Errors::InternalServerError do
  let(:error) { Errors::InternalServerError.new }
  let(:expected_code) { 500 }
  let(:expected_title) { 'Internal Server Error' }
  let(:expected_detail) { 'Something went wrong!! Please try again' }

  it 'should have the correct code' do
    expect(error.code).to eq(expected_code)
  end

  it 'should have the correct title' do
    expect(error.title).to eq(expected_title)
  end

  it 'should have the correct details' do
    expect(error.detail).to eq(expected_detail)
  end

  it 'should have the correct hash output' do
    expect(error.to_hash).to eq(
      {
        code: expected_code,
        title: expected_title,
        detail: expected_detail
      }
    )
  end
end
