require 'rails_helper'
require 'test_error'

RSpec.describe Errors::ErrorContainer do
  let(:error) { TestError.new }

  context 'with a single error' do
    let(:container) { Errors::ErrorContainer.new(error) }

    it 'should have the correct errors stored' do
      expect(container.errors).to eq([error])
    end

    it 'should return the errors as an array' do
      expect(container.to_a).to eq(
        [
          {
            code: 0,
            title: 'My Title',
            detail: 'My details'
          }
        ]
      )
    end
  end

  context 'with multiple errors' do
    let(:container) { Errors::ErrorContainer.new(error,error) }

    it 'should have the correct errors stored' do
      expect(container.errors).to eq([error, error])
    end

    it 'should return the errors as an array' do
      expect(container.to_a).to eq(
        [
          {
            code: 0,
            title: 'My Title',
            detail: 'My details'
          },
          {
            code: 0,
            title: 'My Title',
            detail: 'My details'
          }
        ]
      )
    end
  end
end
