require 'rails_helper'

RSpec.describe Errors::BaseError do
  it 'responds to to_hash' do
    expect(Errors::BaseError.new).to respond_to(:to_hash)
  end
end
