require 'rails_helper'

RSpec.describe Errors::ResourceNotFoundError do
  let(:type) { 'Type' }
  let(:id) { 1 }
  let(:resource_not_found_error) { Errors::ResourceNotFoundError.new(type, id) }

  it 'should have the correct code' do
    expect(resource_not_found_error.code).to eq(404)
  end

  it 'should have the correct title' do
    expect(resource_not_found_error.title).to eq("Resource not found")
  end

  it 'should have the correct details' do
    expect(resource_not_found_error.detail).to eq("You asked for Type with ID:1, but this does not exist")
  end

  it 'should return the correct hash of detail' do
    expect(resource_not_found_error.to_hash).to eq(
      {
        code: 404,
        title: "Resource not found",
        detail: "You asked for Type with ID:1, but this does not exist"
      }
    )
  end
end
