require 'rails_helper'

RSpec.describe RoutineTemplatesController, type: :controller do
  fixtures(:program_templates, :users, :routine_templates)

  let(:creator) { users(:user) }
  let(:program_template) { program_templates(:program_template) }
  let(:attr) do
    {
      "data": {
        "type": "routine_templates",
        "attributes": {
          "name": 'Routine C',
          "description": 'My Routine'
        }
      }
    }
  end
  let(:auth_token) { "Token #{Serializers::JwtSerializer.new(creator).attributes[:jwt]}" }

  before do
    @request.env["Content-Type"] = "application/vnd.api+json"
    @request.env['HTTP_AUTHORIZATION'] = auth_token
  end

  let(:routine_template) { routine_templates(:routine_template) }

  describe "POST#create" do
    it "creates a new routine_template given valid params" do
      expect do
        post(:create, attr.merge(program_template_id: program_template.id))
        expect(response).to have_http_status(:created)
        expect(response_json(response)["data"]["type"]).to eq("routine_templates")
        expect(response_json(response)["data"]["attributes"]["name"]).to eq(attr[:data][:attributes][:name])
        expect(response_json(response)["data"]["attributes"]["creator_id"]).to eq(creator.id)
      end.to change(RoutineTemplate, :count).by(1)
    end

    it 'does not create a new program_template given invalid params' do
      invalid_attr = attr
      invalid_attr[:data][:attributes][:name] = ''
      expect do
        post(:create, invalid_attr.merge(program_template_id: program_template.id))
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response_json(response)["errors"][0]["title"]).to eq("name")
      end.to_not change(RoutineTemplate, :count)
    end
  end

  describe "GET#index" do
    it 'should response success' do
      get(:index, program_template_id: program_template.id)
      expect(response).to have_http_status(:ok)
    end

    it 'should have the correct routine_templates' do
      get(:index, program_template_id: program_template.id)
      expect(response_json(response)["data"]).to_not be_nil
      expect(response_json(response)["data"][0]["type"]).to eq('routine_templates')
      expect(response_json(response)["data"][0]["attributes"]["name"]).to eq(routine_template.name)
    end
  end
end
