require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  let(:attr) do
    {
      "data": {
        "type": "users",
        "attributes": {
          "email": "a@b.com",
          "password": "password",
          "password_confirmation": "password"
        },
        "relationships": {
        },
        "links": {
        },
        "meta": {
        }
      }
    }
  end

  before do
    @request.env["Content-Type"] = "application/vnd.api+json"
  end

  describe "POST#create" do
    it "creates a new user given valid params" do
      expect do
        post(:create, attr)
        expect(response).to have_http_status(:created)
        expect(response_json(response)["data"]["type"]).to eq("users")
        expect(response_json(response)["data"]["attributes"]["email"]).to eq(attr[:data][:attributes][:email])
      end.to change(User, :count).by(1)
    end

    it 'does not create a new user given invalid params' do
      invalid_attr = attr
      invalid_attr[:data][:attributes][:password] = 'invalid'
      expect do
        post(:create, invalid_attr)
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response_json(response)["errors"][0]["title"]).to eq("password_confirmation")
      end.to_not change(User, :count)
    end
  end

  describe "GET#index" do
    let(:user) { User.create(attr[:data][:attributes]) }

    it "responds success" do
      get(:index) do
        expect(response).to have_http_status(:ok)
      end
    end

    it 'lists the user' do
      get(:index) do
        expect(response_json(response)["data"]).to be_present
        expect(response_json(response)["data"][0]["email"]).to eq(attr[:data][:attributes][:email])
      end
    end
  end

  describe "GET#show" do
    let(:user) { User.create(attr[:data][:attributes]) }

    it 'responds success' do
      get(:show, id: user.id) do
        expect(response).to have_http_status(:ok)
      end
    end

    it 'contains the user' do
      get(:show, id: user.id) do
        expect(response_json(response)["data"]["type"]).to eq("users")
        expect(response_json(response)["data"]["attributes"]["email"]).to eq(attr[:data][:attributes][:email])
      end
    end

    it 'returns a 404 if the user does not exist' do
      get(:show, id: user.id + 1) do
        expect(response).to have_http_status(:resource_not_found)
      end
    end
  end

  describe 'POST#login' do
    let(:user) { User.create(attr[:data][:attributes]) }

    let(:login_attr) do
      {
        "data": {
          "type": "users",
          "attributes": {
            "email": "a@b.com",
            "password": "password"
          }
        }
      }
    end

    before { user }

    context 'given valid attributes' do
      it 'responds with success' do
        post(:login, login_attr)
        expect(response).to have_http_status(200)
      end

      it 'returns a json web token in the payload' do
        post(:login, login_attr)
        expect(response_json(response)["data"]["type"]).to eq("auth_token")
        expect(response_json(response)["data"]["attributes"]["jwt"]).to be_present
      end
    end

    context 'given invalid attributes' do
      it 'responds with Unauthorized' do
        invalid_attr = login_attr
        invalid_attr[:data][:attributes][:password] = 'invalid'

        post(:login, invalid_attr)
        expect(response).to have_http_status(:unauthorized)
        expect(response_json(response)['errors']).to be_present
      end
    end
  end

  describe "GET#logged_in_user" do
    let(:user) { User.create(attr[:data][:attributes]) }
    let(:serializer) { Serializers::JwtSerializer.new(user) }
    let(:token) { serializer.attributes[:jwt] }

    context 'given a valid auth token' do
      it 'should return information about the correct user' do
        @request.env['HTTP_AUTHORIZATION'] = "Token #{token}"
        get(:logged_in_user)
        expect(response).to have_http_status(:ok)
        expect(response_json(response)["data"]["type"]).to eq("users")
        expect(response_json(response)["data"]["attributes"]["email"]).to eq(attr[:data][:attributes][:email])
      end
    end

    context 'given an invalid auth token' do
      it 'should respond with unauthorised' do
        @request.env['HTTP_AUTHORIZATION'] = 'Token invalid'
        get(:logged_in_user)
        expect(response).to have_http_status(:unauthorized)
        expect(response_json(response)['errors']).to be_present
      end
    end
  end
end
