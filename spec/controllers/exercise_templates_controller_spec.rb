require 'rails_helper'

RSpec.describe ExerciseTemplatesController, type: :controller do
  fixtures(:routine_templates, :users)

  let(:creator) { users(:user) }
  let(:routine_template) { routine_templates(:routine_template) }

  let(:attr) do
    {
      "data": {
        "type": "exercise_template",
        "attributes": {
          "exercise_name": "Bench Press",
          "starting_criteria": {
            "weight": 20,
            "sets": 5
          },
          "completion_criteria": {
            "threshold": 3,
            "weight": 120
          },
          "progression_criteria": {
            "threshold": 1,
            "weight": 2.5,
            "reps": 0
          },
          "failure_criteria": {
            "threshold": 3,
            "weight": 2.5,
            "reps": 0
          }
        }
      }
    }
  end
  let(:auth_token) { "Token #{Serializers::JwtSerializer.new(creator).attributes[:jwt]}" }

  before do
    @request.env["Content-Type"] = "application/vnd.api+json"
    @request.env['HTTP_AUTHORIZATION'] = auth_token
  end

  describe "GET #index" do
    it 'should response success' do
      get(:index, routine_template_id: routine_template.id)
      expect(response).to have_http_status(:ok)
    end

    it "assigns all exercise_templates as @exercise_templates" do
      get :index, { routine_template_id: routine_template.id }
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new ExerciseTemplate" do
        expect do
          post :create, attr.merge(routine_template_id: routine_template.id)
          expect(response).to have_http_status(:created)
          expect(response_json(response)["data"]["type"]).to eq("exercise_templates")
          expect(response_json(response)["data"]["attributes"]["exercise_name"]).to eq(attr[:data][:attributes][:exercise_name])
          expect(response_json(response)["data"]["attributes"]["creator_id"]).to eq(creator.id)
        end.to change(ExerciseTemplate, :count).by(1)
      end
    end

    context "with invalid params" do
      it 'does not create a new exercise template' do
        invalid_attr = attr
        invalid_attr[:data][:attributes][:exercise_name] = ''
        expect do
          post(:create, invalid_attr.merge(routine_template_id: routine_template.id))
          expect(response).to have_http_status(:unprocessable_entity)
          expect(response_json(response)["errors"][0]["title"]).to eq("exercise_name")
        end.to_not change(ExerciseTemplate, :count)
      end
    end
  end
end
