require 'rails_helper'

RSpec.describe ProgramTemplatesController, type: :controller do
  fixtures(:program_templates, :users)

  let(:creator) { users(:user) }
  let(:attr) do
    {
      "data": {
        "type": "program_templates",
        "attributes": {
          "name": "StrongLifts 5x5",
          "acronym": "SL5x5",
          "description": "Loads of useful stuff!",
          "suggested_length": "2 weeks"
        }
      }
    }
  end
  let(:auth_token) { "Token #{Serializers::JwtSerializer.new(creator).attributes[:jwt]}" }

  before do
    @request.env["Content-Type"] = "application/vnd.api+json"
    @request.env['HTTP_AUTHORIZATION'] = auth_token
  end

  let(:program_template) { program_templates(:program_template) }

  describe "POST#create" do
    it "creates a new program_template given valid params" do
      expect do
        post(:create, attr)
        expect(response).to have_http_status(:created)
        expect(response_json(response)["data"]["type"]).to eq("program_templates")
        expect(response_json(response)["data"]["attributes"]["name"]).to eq(attr[:data][:attributes][:name])
        expect(response_json(response)["data"]["attributes"]["creator_id"]).to eq(creator.id)
      end.to change(ProgramTemplate, :count).by(1)
    end

    it 'does not create a new program_template given invalid params' do
      invalid_attr = attr
      invalid_attr[:data][:attributes][:name] = ''
      expect do
        post(:create, invalid_attr)
        expect(response).to have_http_status(:unprocessable_entity)
        expect(response_json(response)["errors"][0]["title"]).to eq("name")
      end.to_not change(ProgramTemplate, :count)
    end
  end

  describe "GET#index" do
    it 'should response success' do
      get(:index)
      expect(response).to have_http_status(:ok)
    end

    it 'should have the correct program templates' do
      get(:index)
      expect(response_json(response)["data"]).to_not be_nil
      expect(response_json(response)["data"][0]["type"]).to eq('program_templates')
      expect(response_json(response)["data"][0]["attributes"]["name"]).to eq(program_template.name)
    end
  end

  describe "GET#show" do
    let(:program_template) { program_templates(:program_template) }

    it 'should response success' do
      get(:show, id: program_template.id)
      expect(response).to have_http_status(:ok)
    end

    it 'should have the correct program templates' do
      get(:show, id: program_template.id)
      expect(response_json(response)["data"]).to_not be_nil
      expect(response_json(response)["data"]["id"]).to eq(program_template.id.to_s)
      expect(response_json(response)["data"]["type"]).to eq('program_templates')
      expect(response_json(response)["data"]["attributes"]["name"]).to eq(program_template.name)
    end

    it 'should respond with a 404 not found when trying to acces an non existent ID' do
      get(:show, id: 99999)
      expect(response).to have_http_status(:not_found)
    end
  end

  describe 'GET#edit' do
    let(:program_template) { program_templates(:program_template) }

    it 'should respond success' do
      get(:edit, id: program_template.id)
      expect(response).to have_http_status(:ok)
    end

    it 'should have the correct program template' do
      get(:edit, id: program_template.id)
      expect(response_json(response)["data"]).to_not be_nil
      expect(response_json(response)["data"]["id"]).to eq(program_template.id.to_s)
      expect(response_json(response)["data"]["type"]).to eq('program_templates')
      expect(response_json(response)["data"]["attributes"]["name"]).to eq(program_template.name)
    end

    it 'should respond with a 404 not found when trying to acces an non existent ID' do
      get(:edit, id: 99999)
      expect(response).to have_http_status(:not_found)
    end
  end

  describe 'PATCH#update' do
    let(:program_template) do
      template = program_templates(:program_template)
      template.creator = creator
      template.save
      template
    end
    let(:new_name) { 'New Name' }
    let(:update_attr) do
      attr.deep_dup.tap do |new_attr|
        new_attr[:data].merge!(attributes: { name: new_name })
      end
    end

    it 'should respond ok' do
      patch(:update, update_attr.merge(id: program_template.id))
      expect(response).to have_http_status(:ok)
    end

    it 'should update the resource' do
      patch(:update, update_attr.merge(id: program_template.id))
      expect(program_template.reload.name).to eq(new_name)
    end

    it 'should have the correct updated attributes' do
      patch(:update, update_attr.merge(id: program_template.id))
      expect(response_json(response)["data"]["type"]).to eq("program_templates")
      expect(response_json(response)["data"]["attributes"]["name"]).to eq(new_name)
    end

    it 'should respond with a 404 not found when trying to update an non existent ID' do
      patch(:update, update_attr.merge(id: 99999))
      expect(response).to have_http_status(:not_found)
    end

    it 'should not update the resource given invalid params' do
      invalid_attr = update_attr.deep_dup.tap do |new_attr|
        new_attr[:data].merge!(attributes: { name: '' })
      end

      patch(:update, invalid_attr.merge(id: program_template.id))
      expect(response).to have_http_status(:unprocessable_entity)
      expect(response_json(response)["errors"]).to be_present
    end
  end

  describe 'DELETE#destroy' do
    let(:program_template) { program_templates(:program_template) }

    it 'should destroy the program_template' do
      expect do
        delete(:destroy, { id: program_template.id })
      end.to change(ProgramTemplate, :count).by(-1)
    end

    it 'should response success' do
      delete(:destroy, { id: program_template.id })
      expect(response).to have_http_status(:no_content)
    end

    it 'should response with a 404 not found when trying to delete a non existent id' do
      delete(:destroy, { id: 99999 })
      expect(response).to have_http_status(:not_found)
    end
  end
end
