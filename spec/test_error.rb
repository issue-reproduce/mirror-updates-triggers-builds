class TestError < Errors::BaseError
  def initialize
    @code = 0
    @title = 'My Title'
    @detail = 'My details'
  end
end
