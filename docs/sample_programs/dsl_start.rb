program do
  name 'StrongLifts 5x5'
  acronym 'SL5x5'
  suggested_length 3.months

  routine do
    name 'Routine A'

    exercise :squat do
      name 'Squat'
      acronym 'SQ'

      sets 5 do
        reps 5
      end

      increment :range => (0..9999), :unit => :lbs do
        increase 5
      end
      increment :range => (0..9999), :unit => :kg do
        increase 2.5
      end
    end

    exercise :bench_press do
      name 'Bench Press'
      acronym 'BP'

      sets 5 do
        reps 5
      end
      
      increment :range => (0..9999), :unit => :lbs do
        increase 5
      end

      increment :range => (0..9999), :unit => :kgs do
        increase 2.5
      end
    end

    exercise :barbell_row do
      name 'Barbell Row'
      acronym 'BR'

      sets 5 do
        reps 5
      end
      
      increment :range => (0..9999), :unit => :lbs do
        increase 5
      end

      increment :range => (0..9999), :unit => :kgs do
        increase 2.5
      end
    end
  end

  routine do
    name 'Routine B'

    exercise :squat

    exercise :overhead_press do
      name 'Overhead Press'
      acronym 'OHP'

      sets 5 do
        reps 5
      end
      
      increment :range => (0..9999), :unit => :lbs do
        increase 5
      end

      increment :range => (0..9999), :unit => :kgs do
        increase 2.5
      end
    end

    exercise :deadlift do
      name 'Deadlift'
      acronym 'DL'

      sets 1 do
        reps 5
      end
      
      increment :range => (0..225), :unit => :lbs do
        increase 5
      end
      increment :range => (255..9999), :unit => :lbs do
        increase 2.5
      end

      increment :range => (0..100), :unit => :kgs do
        increase 2.5
      end
      increment :range => (100..9999), :unit => :kgs do
        increase 1.25
      end
    end
  end
end
