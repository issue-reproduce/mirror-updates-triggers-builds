Program: Madcow Fitness

Starting Criteria: SL 5x5

Completion criteria: None
Suggested Length: 3 months - Forever

Routine A:

Exercise 1:
  Name: Squat
  No Sets: 1
    Reps per Set: 5
  Increment ranges:
    0-%: +5lbs per week

  Warmup Sets:
    No Sets: 1
      Reps Per Set: 5
    Set weight: 50% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 62.5% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 75% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 87.5% of working weight

Exercise 2:
  Name: Bench Press
  No Sets: 1
    Reps Per Set: 5
  Increment ranges:
    0-%: +5lbs per week

  Warmup Sets:
    No Sets: 1
      Reps Per Set: 5
    Set weight: 50% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 62.5% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 75% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 87.5% of working weight

Exercise 3:
  Name: Barbell Row
  No Sets: 5
    Reps per set: 5
  Increment ranges:
    0-%: +5lbs

  Warmup Sets:
    No Sets: 1
      Reps Per Set: 5
    Set weight: 50% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 62.5% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 75% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 87.5% of working weight

Routine B:

Exercise 1:
  Name: Light Squat
  No Sets: 2
    Reps per Set: 5

  Warmup Sets:
    No Sets: 1
      Reps Per Set: 5
    Set weight: 67.5% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 82.5% of working weight

Exercise 2:
  Name: Overhead Press
  No Sets: 1
    Reps Per Set: 5
  Increment ranges:
    0-%: +5lbs per week

  Warmup Sets:
    No Sets: 1
      Reps Per Set: 5
    Set weight: 50% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 62.5% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 75% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 87.5% of working weight

Exercise 3:
  Name: Deadlift
  No Sets: 1
    Reps per set: 5
  Increment ranges:
    0-%: +5lbs per week

  Warmup Sets:
    No Sets: 1
      Reps Per Set: 5
    Set weight: 50% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 62.5% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 75% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 87.5% of working weight

Routine C:

Exercise 1:
  Name: Heavy Squat
  No Sets: 1
    Reps per Set: 3

  Warmup Sets:
    No Sets: 1
      Reps Per Set: 5
    Set weight: 50% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 60% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 75% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 85% of working weight

  Backoff Sets:
    No Sets: 1
      Reps Per Set: 5
    Set weight: 60% of working weight

Exercise 2:
  Name: Heavy Bench Press
  No Sets: 1
    Reps Per Set: 3

  Warmup Sets:
    No Sets: 1
      Reps Per Set: 5
    Set weight: 50% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 60% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 75% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 85% of working weight

  Backoff Sets:
    No Sets: 1
      Reps Per Set: 5
    Set weight: 60% of working weight

Exercise 3:
  Name: Heavy Barbell Row
  No Sets: 1
    Reps per set: 5

  Warmup Sets:
    No Sets: 1
      Reps Per Set: 5
    Set weight: 50% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 60% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 75% of working weight

    No Sets: 1
      Reps Per Set: 5
    Set weight: 85% of working weight

  Backoff Sets:
    No Sets: 1
      Reps Per Set: 5
    Set weight: 60% of working weight
