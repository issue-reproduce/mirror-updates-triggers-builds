class CreateExerciseTemplates < ActiveRecord::Migration
  def change
    create_table :exercise_templates do |t|
      t.string :exercise_name
      t.text :starting_criteria
      t.text :completion_criteria
      t.text :progression_criteria
      t.text :failure_criteria
      t.integer :routine_template_id
      t.integer :creator_id
    end
  end
end
