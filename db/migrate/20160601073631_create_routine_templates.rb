class CreateRoutineTemplates < ActiveRecord::Migration
  def change
    create_table :routine_templates do |t|
      t.string :name, null:false
      t.text :description
      t.integer :program_template_id
      t.integer :creator_id

      t.timestamps null: false
    end
  end
end
