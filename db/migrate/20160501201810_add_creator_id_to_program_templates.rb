class AddCreatorIdToProgramTemplates < ActiveRecord::Migration
  def change
    add_column :program_templates, :creator_id, :integer
  end
end
