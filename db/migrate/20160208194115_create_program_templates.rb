class CreateProgramTemplates < ActiveRecord::Migration
  def change
    create_table :program_templates do |t|
      t.string :name
      t.string :acronym
      t.text :description
      t.integer :suggested_length
    end
  end
end
