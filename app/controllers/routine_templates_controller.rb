class RoutineTemplatesController < ApplicationController
  before_filter :find_program_template_or_redirect

  def create
    @routine_template = @program_template.routine_templates.new(routine_template_params)
    @routine_template.creator = current_user
    if @routine_template.save
      render_routine_template(:created)
    else
      render_validation_errors @routine_template.errors
    end
  end

  def index
    render json: @program_template.routine_templates, status: :ok
  end

  private

  def routine_template_params
    params.require(:data).require(:attributes).permit(:name, :description)
  end

  def render_routine_template(status = :ok)
    render json: @routine_template, status: status
  end

  def find_program_template_or_redirect
    @program_template = ProgramTemplate.find_by_id(params[:program_template_id])
    render_resource_not_found(params[:type], params[:program_template_id]) unless @program_template
  end
end
