class ExerciseTemplatesController < ApplicationController
  before_action :find_routine_template_or_redirect

  def index
    render json: @routine_template.exercise_templates
  end

  def create
    @exercise_template = @routine_template.exercise_templates.new(exercise_template_params)
    @exercise_template.creator = current_user

    if @exercise_template.save
      render_exercise_template(:created)
    else
      render_validation_errors @exercise_template.errors
    end
  end

  private

  def render_exercise_template(status = :ok)
    render json: @exercise_template, status: status
  end

  def set_exercise_template
    @exercise_template = ExerciseTemplate.find(params[:id])
  end

  def exercise_template_params
    params.require(:data).require(:attributes).permit(:exercise_name,
                                                      starting_criteria: [:weight, :sets],
                                                      completion_criteria: [:threshold, :weight],
                                                      progression_criteria: [:threshold, :weight, :reps],
                                                      failure_criteria: [:threshold, :weight, :reps])
  end

  def find_routine_template_or_redirect
    @routine_template = RoutineTemplate.find_by_id(params[:routine_template_id])
    render_resource_not_found(params[:type], params[:routine_template_id]) unless @routine_template
  end
end
