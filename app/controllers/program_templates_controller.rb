class ProgramTemplatesController < ApplicationController
  before_filter :find_program_template_or_redirect, except: [:index, :create]

  def index
    render json: ProgramTemplate.all, status: :ok
  end

  def show
    render_program_template
  end

  def create
    @program_template = ProgramTemplate.new(program_template_params)
    @program_template.creator = current_user
    if @program_template.save
      render_program_template(:created)
    else
      render_validation_errors @program_template.errors
    end
  end

  def edit
    render_program_template
  end

  def update
    if @program_template.update(program_template_params)
      render_program_template
    else
      render_validation_errors @program_template.errors
    end
  end

  def destroy
    if @program_template.destroy
      render_program_template :no_content
    else
      render_internal_server_error
    end
  end

  private

  def program_template_params
    params.require(:data).require(:attributes).permit(:name, :acronym, :description, :suggested_length, :creator_id)
  end

  def render_program_template(status = :ok)
    render json: @program_template, status: status
  end

  def find_program_template_or_redirect
    @program_template = ProgramTemplate.find_by_id(params[:id])
    render_resource_not_found(params[:type], params[:id]) unless @program_template
  end
end
