class ApplicationController < ActionController::API
  include ActionController::HttpAuthentication::Token::ControllerMethods

  before_action :authenticate

  def render_resource_not_found(type, id)
    render_error Errors::ResourceNotFoundError.new(type, id), :not_found
  end

  def render_unauthorized
    render_error Errors::UnauthorizedError.new, :unauthorized
  end

  def render_forbidden
    render_error Errors::ForbiddenError.new, :forbidden
  end

  def render_internal_server_error
    render_error Errors::InternalServerError.new, :internal_server_error
  end

  def render_error(error, status = :internal_server_error)
    error_container = Errors::ErrorContainer.new(error)
    render json: Serializers::ErrorSerializer.new(error_container).serialize, status: status
  end

  def render_validation_errors(errors)
    error_container = Errors::RailsValidationErrorContainer.new(errors)
    render json: Serializers::ErrorSerializer.new(error_container).serialize, status: :unprocessable_entity
  end

  def current_user
    @current_user
  end

  def authenticate
    @current_user = authenticate_with_http_token do |token, options|
      User.by_auth_token(token)
    end || render_unauthorized
  end
end
