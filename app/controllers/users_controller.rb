class UsersController < ApplicationController
  skip_filter :authenticate, only: [:create, :login]

  def index
    render json: User.all, status: :ok
  end

  def create
    user = User.new(user_params)
    if user.save
      render json: user, status: :created
    else
      render_validation_errors(user.errors)
    end
  end

  def show
    user = User.find_by_id(params[:id])
    if user
      render json: user, status: :ok
    else
      render_resource_not_found(params[:type], params[:id])
    end
  end

  def login
    user = User.authenticate(user_params[:email], user_params[:password])
    if user
      render json: user, serializer: Serializers::JwtSerializer, status: :ok
    else
      render_unauthorized
    end
  end

  def logged_in_user
    render json: current_user, status: :ok
  end

  private

  def user_params
    params.require(:data).require(:attributes).permit(:email, :password, :password_confirmation)
  end
end
