class RoutineTemplateSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :program_template_id, :creator_id
end
