class ExerciseTemplateSerializer < ActiveModel::Serializer
  attributes :id,
    :exercise_name,
    :creator_id,
    :routine_template_id,
    :starting_criteria,
    :progression_criteria,
    :completion_criteria,
    :failure_criteria
end
