class ProgramTemplateSerializer < ActiveModel::Serializer
  attributes :id, :name, :acronym, :description, :suggested_length, :creator_id
end
