class RoutineTemplate < ActiveRecord::Base
  belongs_to :creator, class_name: "User"
  belongs_to :program_template

  has_many :exercise_templates

  validates_presence_of :name, :program_template, :creator
end
