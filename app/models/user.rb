require 'digest/sha2'

class User < ActiveRecord::Base
  attr_accessor :password, :password_confirmation
  validates_uniqueness_of :email
  validates_presence_of :email, :salt, :encrypted_password
  validates_presence_of :password, :password_confirmation, if: -> (user) { user.new_record? } 
  validates_confirmation_of :password, if: -> (user) { user.new_record? } 

  before_validation :generate_password, if: -> (user) { user.new_record? } 

  def self.authenticate(email, password)
    user = User.find_by_email(email)
    if user && user.password_matches?(password)
      user
    else
      false
    end
  end 

  def password_matches?(password)
    self.encrypted_password == User.encrypt_password(password, self.salt)
  end

  private

  def generate_password
    self.salt = User.generate_salt
    self.encrypted_password = User.encrypt_password(self.password, self.salt)
  end 

  def self.generate_salt
    SecureRandom.hex
  end

  def self.encrypt_password(str, salt_str)
    Digest::SHA2.hexdigest("#{str}#{salt_str}")
  end

  def self.by_auth_token(token)
    contents = Serializers::JwtSerializer.deserialize(token)
    if contents
      User.find_by_id contents['user_id']
    else
      nil
    end
  end
end
