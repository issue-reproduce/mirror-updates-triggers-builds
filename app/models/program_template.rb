class ProgramTemplate < ActiveRecord::Base
  validates_presence_of :name,
    :acronym,
    :description,
    :suggested_length,
    :creator
  validates_uniqueness_of :acronym, :name

  belongs_to :creator, class_name: "User"
  has_many :routine_templates
end
