class ExerciseTemplate < ActiveRecord::Base
  belongs_to :routine_template
  belongs_to :creator, class_name: "User"

  validates_presence_of :exercise_name,
    :creator,
    :routine_template,
    :starting_criteria,
    :progression_criteria,
    :completion_criteria,
    :failure_criteria

  serialize :starting_criteria
  serialize :progression_criteria
  serialize :completion_criteria
  serialize :failure_criteria

  validate :validate_nested_criteria

  private

  def validate_nested_criteria
    validate_criteria(:starting_criteria, :weight, :sets)
    validate_criteria(:completion_criteria, :weight, :threshold)
    validate_criteria(:progression_criteria, :weight, :threshold, :reps)
    validate_criteria(:failure_criteria, :weight, :threshold, :reps)
  end

  def validate_criteria(criteria_type, *criteria)
    if values = send(criteria_type)
      criteria.each do |criterion|
        validate_criterion(criteria_type, criterion, values[criterion])
      end
    end
  end

  def validate_criterion(criteria_type, criterion, value)
    if value.nil? || !value.to_i.is_a?(Numeric)
      errors.add(criteria_type, "must have a valid numeric #{criterion.to_s}")
    end
  end
end
